import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { map, Observable } from 'rxjs';
@Injectable()
export class AppService {
  constructor(private httpService: HttpService) {}
  getHello(): string {
    return 'Hello World!';
  }

  findAll(options, header): Observable<any> {
    // let tokenUrl = 'https://api.sandbox.ebay.com/identity/v1/oauth2/token';
    return this.httpService
      .post('https://api.ebay.com/identity/v1/oauth2/token', options, {
        headers: header,
      })
      .pipe(map((response) => response));
  }
}
