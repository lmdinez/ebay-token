import { Controller, Get, Logger, Query, Render } from '@nestjs/common';
import { Observable } from 'rxjs';
import { AppService } from './app.service';
import { ApiCreatedResponse } from '@nestjs/swagger';

const base64 = require('base-64');
const EbayAuthToken = require('ebay-oauth-nodejs-client');

@Controller()
export class AppController {
  private readonly logger = new Logger(AppController.name);

  requestOptions: {
    method: string;
    body: any;
    headers: { 'Content-Type': string; Authorization: string };
  };
  returnedData$: Observable<any>;
  tokenData: [];
  constructor(private readonly appService: AppService) {}

  @Get()
  @Render('index')
  getHello() {
    return null;
  }
  redirectUri = 'https://2b2b-157-49-236-95.ngrok.io/oauth/redirect';
  formBody: any = [];
  @Get('/oauth/redirect')
  @ApiCreatedResponse({ description: 'token creation' })
  async getToken(@Query() query: any) {
    this.logger.log('token initialization started!');
    const details = {
      grant_type: 'authorization_code',
      redirect_uri: this.redirectUri,
      code: query.code,
    };

    console.log('code', query.code);
    for (const property in details) {
      const encodedKey = encodeURIComponent(property);
      const encodedValue = encodeURIComponent(details[property]);
      this.formBody.push(encodedKey + '=' + encodedValue);
    }
    this.formBody = this.formBody.join('&');

    return this.get();
  }

  get() {
    const headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
      Authorization:
        'Basic ' +
        base64.encode(
          'Dineshkl-AuthCode-PRD-81a0826f6-e4c3343c:PRD-1a0826f65767-da15-4a4e-86c5-0d26',
        ),
    };
    this.appService.findAll(this.formBody, headers).subscribe((res: any) => {
      const tokenData = res['data'];
      console.log('token', tokenData);
      this.logger.log('token generate completed!');
    });
  }
}
